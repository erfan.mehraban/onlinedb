import json

class DocumentFilter:
    """
    for filtering queryset of documents in collection

    use filter method
    """

    @staticmethod
    def filter(queryset, filters):
        """
        filter queryset of documents by given filters
        :param queryset: queryset of documents
        :param filters: array of filters wich is in this protocol:
        {
            "key" : str. key in jsonField
            "op" : str. operand for filtering
            "val" : str. value to consider
        }
        """
        for user_filter in filters:
            user_filter = json.loads(user_filter)
            filter_func = filter_functions[user_filter["op"]]
            queryset = filter_func(queryset, user_filter["key"], user_filter["val"])
        return queryset

    @staticmethod
    def equal(queryset, key, value):
        return queryset.filter(data__contains={key : value})
    
    @staticmethod
    def greater(queryset, key, value):
        key = "data__"+key+"__gt"
        filter_data = {key:value}
        return queryset.filter(**filter_data)

    @staticmethod
    def lower(queryset, key, value):
        key = "data__"+key+"__lt"
        filter_data = {key:value}
        return queryset.filter(**filter_data)

filter_functions = {
    "=" : DocumentFilter.equal,
    ">" : DocumentFilter.greater,
    "<" : DocumentFilter.lower,
}
