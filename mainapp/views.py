import json

from rest_framework import mixins, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response

from .models import Collection, Document
from .serializers import CollectionSerializer, DocumentSerializers
from .document_filter import DocumentFilter

class CollectionViewsets(viewsets.ModelViewSet):
    """
    getting list of user collection or add/delete/edit one of them
    """

    serializer_class = CollectionSerializer
    def get_queryset(self):
        return self.request.user.collection_set.all()

    def retrieve(self, request, pk):
        """
        getting all documents in collection ro filter them by filter parameter
        
        example: http://127.0.0.1:8000/collection/7/?filter={%22key%22:%22key%22,%22op%22:%22%3C%22,%22val%22:7}&filter={%22key%22:%22key%22,%22op%22:%22%3E%22,%22val%22:1}
        """
        user_filters = request.query_params.getlist("filter", None)
        queryset = self.get_queryset().get(id=pk).document_set
        if user_filters:
            queryset = DocumentFilter.filter(queryset, user_filters)
        serializer = DocumentSerializers(queryset, many=True)
        return Response(serializer.data)

        # return super().retrieve(request, pk)
        

class DocumentViewsets(mixins.RetrieveModelMixin, mixins.CreateModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    """
    getting data in document
    """
    serializer_class = DocumentSerializers

    def get_queryset(self):
        queryset = Document.objects.filter(collection__user=self.request.user)
        return queryset
    
    def create(self, request, *args, **kwargs):
        """
        add new document to collection
        """
        if self.request.user == Collection.objects.get(id=request.data["collection"]).user:
            return super().create(request, *args, **kwargs)
        raise PermissionDenied
    
    def destroy(self, request, pk=None):
        """
        delete whole document
        """
        if self.request.user == Document.objects.get(id=pk).collection.user:
            return super().destroy(request)
        raise PermissionDenied
