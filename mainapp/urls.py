# from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers
from django.conf.urls import include, url
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token
from . import views
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Online Database')
router = routers.SimpleRouter()
router.trailing_slash='/?' #optional trailing slash
router.register(r'collection', views.CollectionViewsets, base_name="Collection")
collection_router = routers.NestedSimpleRouter(router, r'collection', lookup='domain')
collection_router.trailing_slash='/?' #optional trailing slash
collection_router.register(r'doc', views.DocumentViewsets, base_name="Document")
urlpatterns = [
    url(r'', include(router.urls), name='router'),
    url(r'', include(collection_router.urls), name='doc'),
    url(r'^api-token-auth/', obtain_jwt_token, name='jwt-token-login'),
    url(r'', schema_view),
    ]
