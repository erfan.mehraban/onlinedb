from django.contrib import admin
from .models import Document, Collection

admin.site.register(Document)
admin.site.register(Collection)