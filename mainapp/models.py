from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User

class Collection(models.Model):
    name = models.CharField(max_length = 100, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    
class Document(models.Model):
    data = JSONField()
    collection = models.ForeignKey('Collection', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)+str(self.data)
