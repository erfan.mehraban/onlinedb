from django.test import TestCase
from django.contrib.auth.models import User
from .models import *
from rest_framework.test import APIClient
from django.urls import reverse

class OnlineDBTest(TestCase):
    """ test module for testing main app of online database """

    def setUp(self):
        self.client = APIClient()
        test_user1 = User.objects.create_user(username="testuser1", password="testpassword")
        test_user2 = User.objects.create_user(username="testuser2", password="testpassword")
        self.client.token = self.client.post(reverse('jwt-token-login'),
            {'username':test_user1.username, 'password':"testpassword"},
            format='json').data["token"]
        self.client.credentials(HTTP_AUTHORIZATION=' JWT '+self.client.token)
        col1 = Collection.objects.create(user=test_user1, name="coll1")
        col2 = Collection.objects.create(user=test_user1, name="coll2")
        col3 = Collection.objects.create(user=test_user2, name="coll3")
        docs = []
        for i in range(10):
            docs.append(Document.objects.create(data={"key":i}, collection=col1))

        # for i in range(10):
        #     Document.objects.create(data={"key":i},collection=col2)
    
    def test_get_collections(self):
        response = self.client.get('/collection/')
        assert response.status_code == 200
        self.assertEqual(len(response.data), 2)
        print(response.data)