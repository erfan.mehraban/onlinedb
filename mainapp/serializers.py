from rest_framework.serializers import ModelSerializer, CurrentUserDefault, CharField, PrimaryKeyRelatedField
from .models import Collection, Document
from rest_framework.exceptions import APIException

DATA_LIMIT_SIZE = 1024*16 # 16mb

class DocumentSerializers(ModelSerializer):
    collection = PrimaryKeyRelatedField(queryset=Collection.objects.all(), write_only=True)
    class Meta:
        model = Document
        fields = ('id', 'data', 'collection')
    
    def save(self):
        """ save data if it is below limit size """
        if len(self.validated_data["data"]) > DATA_LIMIT_SIZE:
            raise APIException(f"data limit reached: {DATA_LIMIT_SIZE}")
        super().save()

class CollectionSerializer(ModelSerializer):
    class Meta:
        model = Collection
        fields = ('name', 'id')

    def create(self, validated_data):
        """save new document by login user """
        user = self.context['request'].user
        name = validated_data['name']
        new_instance = Collection(name=name, user=user)
        new_instance.save()
        return new_instance