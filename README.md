# Simple Online Database

This is simple online database which users can save json data online and retrive it later and filter them via **REST api**.

this project based on django-rest-framework and postgresql

## Concepts

user data (which should be json) stored in document. documents storing in collection and every user has some collection.

## How to run?

run django normally:
```bash
python3 manage.py runserver
```

This project has browsable api and you can browse it directly in root url.

you can see schema in ```[url]/schema/```

getting token endpoint is at ```[url]/api-token-auth/```

collections are accessible at ```[url]/collection/```

and documents are accessible at ```[url]/collection/<coll_pk>/doc/<pk>```


### TODO

- logger
- unit test
- use tuple for filtering instead of json
- rewrite filter class for fit in backend.filter (is it possible at all?)